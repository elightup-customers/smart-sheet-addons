<?php
/**
 * Plugin Name: Smart Addons
 * Description: Addons for website.
 * Author:      GretaThemes
 * Author URI:  https://gretathemes.com
 * Version:     1.0.0
 */

/**
 * Main plugin class.
 */
class Smart_Addons {
	/**
	 * Constructor
	 *
	 * Add actions for methods that define constants, load translation and load includes.
	 *
	 * @since 0.1
	 * @access public
	 */
	public function __construct() {
		define( 'SMART_DIR', __DIR__ );
		define( 'SMART_URL', plugin_dir_url( __FILE__ ) );
		define( 'SMART_IMG_TEMPLATE_URL', plugin_dir_url( __FILE__ ) . '/inc/template-pdf' );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue' ) );

		$this->load_files();
	}

	public function load_files() {
		require_once dirname( __FILE__ ) . '/inc/load_template_pdf.php';
		new Load_Template_PDF;
	}

	function enqueue() {
		$screen = get_current_screen();
		if ( is_admin() && ( $screen->id == 'games' ) ) {
			$game_id = get_the_ID();
		}
		wp_enqueue_script( 'smart-admin-js', plugins_url( 'assets/js/admin.js', __FILE__ ), array( 'jquery' ), '1.0.0', true );
		wp_localize_script( 'smart-admin-js', 'ajax_object', array(
			'post_id'  => $game_id,
		) );
	}

}
new Smart_Addons;