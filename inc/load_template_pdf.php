<?php

class Load_Template_PDF {
	public function __construct() {
		add_action( 'template_redirect', [ $this, 'load_template' ] );
	}

	public function load_template() {
		if ( $game_id = filter_input( INPUT_GET, 'game_id', FILTER_SANITIZE_NUMBER_INT ) ) {
			include SMART_DIR . '/inc/template-pdf/index.php';
			die;
		}
	}
}