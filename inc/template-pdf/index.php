<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <title>Product X</title>

    <link href="<?php echo SMART_URL ?>inc/template-pdf/css/css.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, viewport-fit=cover, shrink-to-fit=no">

    <style>
        body {
            font-family: 'Open Sans', sans-serif;
        }

        header {
            display: block;
            max-width: 100%;
        }

        .clear {
            clear: both;
        }

        .row {
            width: 100%;
            display: table;
            content: " ";
            clear: both;
        }

        .c25 {
            width: 24.9%;
            float: left;
        }

        .c30 {
            width: 29.9%;
            float: left;
        }

        .c40 {
            width: 39.3%;
            float: left;
        }

        .c45 {
            width: 44.3%;
            float: left;
        }

        .c50 {
            width: 49.3%;
            float: left;
        }

        .c55 {
            width: 54.3%;
            float: left;
        }

        .c60 {
            width: 59.3%;
            float: left;
        }

        .c70 {
            width: 69.9%;
            float: left;
        }

        .c80 {
            width: 79.9%;
            float: left;
        }

        .c90 {
            width: 89.9%;
            float: left;
        }

        .c100 {
            width: 100%;
            float: left;
        }

        .logo {
            padding: 10px;
        }

        .logo img {
            width: 100%;
        }

        .info {
            padding: 10px;
        }

        h1 {
            padding: 0;
            margin: 0;
        }

        .title {
            padding: 10px;
        }

        .pegi {
            padding: 10px;
        }

        .pegi img {
            width: 100%;
        }

        .image-one,
        .image-two {
            padding: 10px;
        }

        .image-one img,
        .image-two img {
            width: 100%;
        }

        .card {
            padding: 20px;
            border-radius: 6px;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            box-shadow: 0 2px 8px rgba(0, 0, 0, .16);
        }

        .ul-2-col {
            column-count: 3;
        }

        .gallery-image {
            padding: 10px;
        }

        .gallery-image img {
            width: 100%;
        }

        .description,
        .raiting,
        .conentu,
        .pour-commander,
        .quote {
            padding: 10px;
        }

        .plan-marketing {
            padding: 10px;
        }

        .plan-marketing-inner {
            padding: 10px;
            border: 1px #cacaca solid;
        }

        .marketing-images {
            padding: 10px;
            text-align: center;
        }

        .marketing-images img {
            width: 100%;
        }
        .footer {
            color:#fff;
            background: #333;
        }

        .copyright {
            padding: 10px;
        }
        .copyright a {
            margin-left: 20px;
        }
        .copyright img {
            vertical-align: middle;
        }

        .footer-socials {
            padding: 10px;
            list-style: none;
            float: right;
        }

        .footer-socials li {
            display: inline-block;
            margin: 0 15px 0 0;
            font-size: 20px;
            color:#ccc;
        }
        .footer-socials li a {
            color:#fff;
        }
        @media (max-width: 991px) {
            p {
                font-size: 10px;
            }
            .description, .raiting, .conentu, .pour-commander, .quote {
                padding: 5px 10px;
            }
            .description ul {
                padding-left: 20px;
            }
            .raiting h1 {
                font-size: 17px;
            }
            .card {
                padding: 10px 20px;
            }
            .card h2 {
                font-size: 13px;
            }
            .image-one img,
            .image-two img {
                max-height: 195px;
                object-fit: cover;
            }
            h1 {
                padding: 0;
                margin: 0;
                font-size: 20px;
            }
            h2 {
                font-size: 15px;
                margin: 5px 0;
            }
            h4 {
                margin: 5px 0;
            }
            .logo {
                padding: 10px 10px 5px;
            }
            .logo img {
                width: 100%;
                max-height: 70px;
                object-fit: cover;
            }
            .gallery-image {
                padding: 5px;
            }
            .gallery-image img {
                max-height: 100px;
                object-fit: cover;
            }
            .marketing-images {
                padding: 0 10px;
            }
            .marketing-images img {
                max-height: 100px;
                object-fit: cover;
            }
            .image-one, .image-two {
                padding: 0 10px;
            }
            .plan-marketing {
                padding: 5px 10px 0;
            }
            body {
                font-family: 'Open Sans', sans-serif;
                font-size: 11px;
            }
        }
    </style>

</head>

<body>
    <main>
        <div class="row">
            <div class="c45">
                <div class="row">
                    <div class="c40">
                        <div class="logo">
                            <?php
                            $img = get_field( 'publisher', $game_id );
                            ?>
                            <img src="<?php echo $img['publisher_logo']['url']; ?>">
                        </div>
                    </div>
                    <div class="c60">
                        <div class="info">
                            <div>
                                <b>Release Date:</b> <span><?php the_field( 'release_date', $game_id ); ?></span>
                            </div>
                            <div class="info-images">
                                <?php the_field( 'platform__support', $game_id ); ?>
                                <?php the_field( 'game_mode', $game_id ); ?>
                                <?php the_field( 'languages', $game_id ); ?>
                                <!-- <img src="<?php echo SMART_IMG_TEMPLATE_URL ?>/images/50x50.png"> -->
                            </div>
                            <div class="prix-tarif">
                                <b>Prix tarif</b>
                                <span>sdasdsadsada</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c55">
                <div class="row">
                    <div class="c70">
                        <div class="title">
                            <?php if( get_field( 'title', $game_id ) ): ?>
                                <h1><?php the_field( 'title', $game_id ); ?></h1>
                            <?php endif; ?>
                            <p>"Lorem Ipsum is simply dummy"</p>
                        </div>
                    </div>
                    <div class="c30">
                        <div class="pegi">
                            <?php the_field( 'pegi', $game_id ); ?>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <div class="row">
            <div class="c45">
                <div class="image-one">
                    <?php
                    $packaging = get_field( 'packaging_', $game_id );
                    ?>
                    <img src="<?php echo $packaging['url']; ?>">
                </div>
            </div>
            <div class="c55">
                <div class="image-two">
                    <?php
                    $screen_shot = get_field( 'screen_shot', $game_id );
                    ?>
                    <img src="<?php echo $screen_shot['url']; ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="c50">
                <div class="description">
                    <h1>Description</h1>
                    <p><?php the_field( 'description_', $game_id ); ?> </p>
                    <div class="row">
                        <div class="c50">
                            <h2>Points forts</h2>
                            <ul>
                                <li><?php the_field( 'key_usp_1', $game_id ); ?></li>
                                <li><?php the_field( 'key_usp_2', $game_id ); ?></li>
                                <li><?php the_field( 'key_usp_3', $game_id ); ?></li>
                                <li>Key USP 4</li>
                            </ul>
                        </div>
                        <div class="c50">
                            <h2>En details</h2>
                            <ul>
                                <li>
                                <?php
                                $publisher = get_field( 'publisher', $game_id );
                                echo $publisher['publisher_name'];
                                ?>
                                </li>
                                <li><?php the_field( 'genre', $game_id ); ?> > <?php the_field( 'subgenre', $game_id ); ?></li>
                                <li><?php the_field( 'category', $game_id ); ?></li>
                                <li>List Price</li>
                            </ul>
                        </div>
                    </div>
                    <div class="card">
                        <h2>Informations complementaires</h2>
                        <ul class="ul-2-col">
                            <?php the_field( 'details', $game_id ); ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="c50">
                <div class="row">
                    <div class="c50">
                        <div class="gallery-image">
                            <?php
                            $publisher = get_field( 'presse', $game_id );
                            ?>
                            <img src="<?php echo $publisher['presse_logo']['url']; ?>">
                        </div>
                    </div>
                    <div class="c50">
                        <div class="gallery-image">
                            <img src="<?php echo $publisher['presse_logo']['url']; ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="c50">
                        <div class="gallery-image">
                            <img src="<?php echo $publisher['presse_logo']['url']; ?>">
                        </div>
                    </div>
                    <div class="c50">
                        <div class="gallery-image">
                            <img src="<?php echo $publisher['presse_logo']['url']; ?>">
                        </div>
                    </div>
                </div>
                <div class="raiting">
                    <h1>Presse et influenceurs en parlent</h1>
                    <p>
                        <?php
                        echo $publisher['presse_text'];
                        ?>
                    </p>
                </div>
            </div>
            <div class="c100">
                <div class="conentu">
                    <div class="card">
                        <h2>Contentu edition collector</h2>
                        <ul class="ul-2-col">
                            <?php the_field( 'additional_information', $game_id ); ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="c100">
                <div class="plan-marketing">
                    <div class="plan-marketing-inner">
                        <h2>Plan Marketing</h2>
                        <div class="row">
                            <div class="c25">
                                <div class="marketing-images">
                                    <?php
                                    $marketing = get_field( 'Marketing', $game_id );
                                    ?>
                                    <h4>Web</h4>
                                    <img src="<?php echo $marketing['marketing_logo']['url']; ?>">
                                    <p><?php echo $marketing['marketing_text']; ?></p>
                                </div>
                            </div>
                            <div class="c25">
                                <div class="marketing-images">
                                    <h4>Web</h4>
                                    <img src="<?php echo $marketing['marketing_logo']['url']; ?>">
                                    <p><?php echo $marketing['marketing_text']; ?></p>
                                </div>
                            </div>
                            <div class="c25">
                                <div class="marketing-images">
                                    <h4>Web</h4>
                                    <img src="<?php echo $marketing['marketing_logo']['url']; ?>">
                                    <p><?php echo $marketing['marketing_text']; ?></p>
                                </div>
                            </div>
                            <div class="c25">
                                <div class="marketing-images">
                                    <h4>Web</h4>
                                    <img src="<?php echo $marketing['marketing_logo']['url']; ?>">
                                    <p><?php echo $marketing['marketing_text']; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="c50">
                <div class="quote">
                    <p><i><?php the_field( 'baseline', $game_id ); ?></i></p>
                </div>
            </div>
            <div class="c50">
                <div class="pour-commander">
                    <h2>Pour commander</h2>
                </div>
            </div>
        </div>
        <div class="row footer">
            <div class="c100">
                <div class="footer">
                    <div class="row">
                        <div class="c50">
                            <div class="copyright">
                                <?php
                                $img = get_field( 'logo_koch_media', $game_id );
                                ?>
                                <img src="<?php echo $img['url']; ?>">
                                <a><?php the_field( 'copyrights', $game_id ); ?></a>
                            </div>
                        </div>
                        <div class="c50">
                            <ul class="footer-socials">
                                <li><a href="#" target="_blank"><i class="fab fa-facebook-square" aria-hidden="true"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-pinterest" aria-hidden="true"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

<script src="<?php echo SMART_URL ?>/inc/template-pdf/js/de72bb18b7.js" crossorigin="anonymous"></script>
<!-- <link rel="stylesheet" href="<?php echo SMART_URL ?>/inc/template-pdf/css/free-v4-shims.css" media="all">
<link rel="stylesheet" href="<?php echo SMART_URL ?>/inc/template-pdf/css/free-v4-font-face.css" media="all">
<link rel="stylesheet" href="<?php echo SMART_URL ?>/inc/template-pdf/css/free.css" media="all"> -->
<script type="text/javascript">
    window.onload = function() {
        window.print();
    }
</script>
</body></html>